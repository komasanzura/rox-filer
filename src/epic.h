#ifndef _EPIC_H
#define _EPIC_H

void epic_init(void);
void epic_history_init(FilerWindow *filer_window);
void epic_history_destroy(FilerWindow *filer_window);
void epic_history_add(FilerWindow *filer_window, gchar *path, gchar *cursor);
void epic_filer_go_back(FilerWindow *filer_window);
void epic_filer_go_forward(FilerWindow *filer_window);
void history_show_prev_menu(FilerWindow *filer_window);
void history_show_next_menu(FilerWindow *filer_window);

#endif /* _EPIC_H */
