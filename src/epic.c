/*
 * ROX-Filer, filer for the ROX desktop project
 * Copyright (C) 2006, Thomas Leonard and others (see changelog for details).
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* epic.c - many miscellaneous functions by KomasanZura */

#include "config.h"

#include <stdlib.h>
#include <gtk/gtk.h>
#include <string.h>
#include <libxml/parser.h>

#ifdef ROX_WITH_DBUS
#include <dbus/dbus.h>
#endif

#include "global.h"

#include "filer.h"
#include "main.h"
#include "remote.h"
#include "support.h"
#include "epic.h"

#define EPIC_MAX_HISTORY_LENGTH 50
#define EPIC_MAX_HISTORY_MENU_LENGTH 7

/* The scale of the screen for alternative DPI support */
double epic_screen_scale;

/* This hash table maps each filer window to their history data */
GHashTable *epic_history_datas;

/* The structure for a history entry */
struct EpicHistElem {
	FilerWindow *fw; /* The filer window that owns this entry */
	GQueue *history; /* The history list this entry belongs to */
	char *fullpath; /* The path where this entry will send you to when clicked */
	char *display; /* This points inside fullpath, don't free! */
	char *cursor; /* The item where the cursor was before leaving this dir */
	int postmp; /* This value is only valid for the clicked element on a menu */
};

enum EpicHistSource {
	EPIC_HIST_OTHER,		/* Something else */
	EPIC_HIST_BACK,			/* The back button was pressed */
	EPIC_HIST_FORWARD,		/* The forward button was pressed */
	EPIC_HIST_MENU_BACK,		/* Back history menu was pressed */
	EPIC_HIST_MENU_FORWARD		/* Forward history menu was pressed */
};

struct EpicHistData {
	GQueue *prev_history; /* History of previous directories */
	GQueue *next_history; /* History of next dirs, it's filled when going back */
	enum EpicHistSource add_source; /* What was the action that caused the hist add */
	struct EpicHistElem *dummy; /* Needed for a horrible, horrible, hack */
};

#ifdef ROX_WITH_DBUS
static gint epic_close_dbus(gpointer data)
{
	DBusConnection *conn = data;
	dbus_connection_close(conn);
	return 0;
}

static void epic_dbus_reply_msg(DBusConnection *conn, DBusMessage *msg)
{
	DBusMessage *reply;
	DBusMessageIter arg;
	dbus_uint32_t serial;
	dbus_bool_t answer = 1;

	reply = dbus_message_new_method_return(msg);
	dbus_message_iter_init_append(reply, &arg);
	dbus_message_iter_append_basic(&arg, DBUS_TYPE_BOOLEAN, &answer);
	serial = dbus_message_get_serial(msg);
	dbus_connection_send(conn, reply, &serial);
	dbus_connection_flush(conn);
	dbus_message_unref(reply);
}

static gboolean epic_dbus_read_msg(gpointer data)
{
	DBusConnection *conn = data;
	DBusMessage *msg;
	DBusMessageIter arg, sub;
	xmlDocPtr soap;
	xmlNodePtr soapbody;
	int argtype, count, actual_count, i;
	char **arglist, *cmd = NULL;

	dbus_connection_read_write(conn, 0);
	msg = dbus_connection_pop_message(conn);

	if (!msg) {
		return TRUE;
	}

	if (dbus_message_is_method_call(msg, "org.freedesktop.FileManager1", "ShowFolders")) {
		cmd = "OpenDir";
	} else if (dbus_message_is_method_call(msg, "org.freedesktop.FileManager1", "ShowItems")) {
		cmd = "Show";
	} else if (dbus_message_is_method_call(msg, "org.freedesktop.FileManager1", "ShowItemProperties")) {
		cmd = "InfoBox";
	}

	if (!cmd) {
		goto msgunref; /* Unknown command */
	}

	if (!dbus_message_iter_init(msg, &arg)) {
		goto msgunref; /* No arguments, nothing we can do */
	}

	argtype = dbus_message_iter_get_arg_type(&arg);
	while (argtype != DBUS_TYPE_ARRAY && argtype != DBUS_TYPE_INVALID) {
		dbus_message_iter_next(&arg);
		argtype = dbus_message_iter_get_arg_type(&arg);
	}

	if (argtype == DBUS_TYPE_INVALID) {
		goto msgunref; /* No array argument found */
	}

	count = dbus_message_iter_get_element_count(&arg);
	if (!count) {
		goto msgunref;
	}

	actual_count = 0;
	arglist = malloc(count * sizeof (char *));
	if (!arglist) {
		goto msgunref;
	}

	dbus_message_iter_recurse(&arg, &sub);
	for (i = 0; i < count; i++) {
		if (dbus_message_iter_get_arg_type(&sub) == DBUS_TYPE_STRING) {
			char *argstr;
			int arglen, ix;
			dbus_message_iter_get_basic(&sub, &argstr);
			if (strncmp(argstr, "file://", 7) == 0 && argstr[7]) {
				argstr += 7;
			}
			arglen = strlen(argstr);
			ix = actual_count;
			arglist[ix] = malloc(arglen + 1);
			if (!arglist[ix]) {
				goto arglistfree;
			}
			actual_count++;
			memcpy(arglist[ix], argstr, arglen + 1);
			while (arglist[ix][arglen - 1] == '/' && arglen > 1) {
				arglist[ix][arglen - 1] = 0;
				arglen--;
			}
		}
		dbus_message_iter_next(&sub);
	}

	if (!actual_count) {
		goto arglistfree;
	}

	soap = soap_new(&soapbody);
	for (i = 0; i < actual_count; i++) {
		if (strcmp(cmd, "Show") == 0) {
			char *tmp, *dir;
			tmp = g_path_get_dirname(arglist[i]);
			if (tmp[0] == '/')
				dir = NULL;
			else
				dir = pathdup(tmp);
			soap_add(soapbody, cmd, "Directory", dir ? dir : tmp, "Leafname", g_basename(arglist[i]));
			g_free(tmp);
			g_free(dir);
		} else {
			soap_add(soapbody, cmd, "Filename", arglist[i], NULL, NULL);
		}
	}
	xmlFreeDoc(run_soap(soap));
	xmlFreeDoc(soap);

	epic_dbus_reply_msg(conn, msg);

arglistfree:
	for (i = 0; i < actual_count; i++) {
		free(arglist[i]);
	}
	free(arglist);

msgunref:
	dbus_message_unref(msg);
	return TRUE;
}
#endif

/* Called at program startup */
void epic_init() {
#ifdef ROX_WITH_DBUS
	DBusError err;
	DBusConnection *conn;

	dbus_error_init(&err);
	conn = dbus_bus_get(DBUS_BUS_SESSION, &err);
	if (dbus_error_is_set(&err)) {
		dbus_error_free(&err);
		return;
	}
	if (conn) {
		int ret;
		ret = dbus_bus_request_name(conn, "org.freedesktop.FileManager1",
		                            DBUS_NAME_FLAG_REPLACE_EXISTING | DBUS_NAME_FLAG_DO_NOT_QUEUE, &err);
		if (dbus_error_is_set(&err)) {
			dbus_error_free(&err);
			return;
		}
		if (ret == DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER) {
			gtk_quit_add(0, epic_close_dbus, conn);
			g_timeout_add(200, epic_dbus_read_msg, conn);
		}
	}
#endif
	epic_screen_scale = gdk_screen_get_resolution(gdk_screen_get_default()) / 96.0;
	epic_history_datas = g_hash_table_new(NULL, NULL);
}

/* Set the path a history entry points to, and the value that will be displayed
 * on the menu as well */
void epic_hist_elem_set_path(struct EpicHistElem *elem, char *path) {
	if (elem->fullpath) {
		g_free(elem->fullpath);
	}
	elem->fullpath = g_strdup(path);
	if (!(elem->display = strrchr(elem->fullpath, '/')) ||
	    (elem->display == elem->fullpath) ||
	    !(++elem->display)) {
		elem->display = elem->fullpath;
	}
}

/* Create a new history entry */
struct EpicHistElem *epic_hist_elem_new(FilerWindow *fw, GQueue *history,
				        char *path, char *cursor) {
	struct EpicHistElem *elem = g_malloc(sizeof (struct EpicHistElem));
	if (elem) {
		elem->fw = fw;
		elem->history = history;
		elem->fullpath = NULL;
		epic_hist_elem_set_path(elem, path);
		elem->postmp = -1;
		elem->cursor = cursor ? g_strdup(cursor) : NULL;
	}
	return elem;
}

/* Destroy the history entry */
void epic_hist_elem_free(struct EpicHistElem *elem) {
	if (elem) {
		g_free(elem->fullpath);
		if (elem->cursor) {
			g_free(elem->cursor);
		}
		g_free(elem);
	}
}

/* New filer window, create new history data for it */
void epic_history_init(FilerWindow *filer_window) {
	struct EpicHistData *data;
	
	if (!epic_history_datas) {
		return;
	}

	data = g_malloc(sizeof (struct EpicHistData));
	if (data) {
		data->prev_history = g_queue_new();
		data->next_history = g_queue_new();
		data->add_source = EPIC_HIST_OTHER;
		data->dummy = NULL;
	}
	g_hash_table_insert(epic_history_datas, filer_window, data);
}

void epic_history_clear(GQueue *history) {
	while (history->length > 0) {
		epic_hist_elem_free(g_queue_pop_tail(history));
	}
}

/* Filer window is going away, remove its data */
void epic_history_destroy(FilerWindow *fw) {
	struct EpicHistData *data = NULL;

	if (epic_history_datas) {
		data = g_hash_table_lookup(epic_history_datas, fw);
		g_hash_table_remove(epic_history_datas, fw);
	}
	
	if (data) {
		if (data->prev_history) {
			epic_history_clear(data->prev_history);
			g_queue_free(data->prev_history);
		}
		if (data->next_history) {
			epic_history_clear(data->next_history);
			g_queue_free(data->next_history);
		}
		g_free(data);
	}
}

/* We've pressed back, return the item to go to */
struct EpicHistElem *get_elem(FilerWindow *fw, enum EpicHistSource source)
{
	GQueue *history = NULL;
	struct EpicHistData *data = NULL;

	if (epic_history_datas) {
		data = g_hash_table_lookup(epic_history_datas, fw);
		if (!data) {
			return NULL;
		}
		switch (source) {
		case EPIC_HIST_BACK:
		case EPIC_HIST_MENU_BACK:
			history = data->prev_history;
			break;
		case EPIC_HIST_FORWARD:
		case EPIC_HIST_MENU_FORWARD:
			history = data->next_history;
			break;
		}
	}

	if (history && history->length > 0) {
		data->add_source = source;
		return g_queue_pop_tail(history);
	} else {
		return NULL;
	}
}

void
epic_filer_go_back(FilerWindow *filer_window)
{
	struct EpicHistElem *dest = get_elem(filer_window, EPIC_HIST_BACK);
	if (dest) {
		filer_change_to(filer_window, dest->fullpath, dest->cursor);
		epic_hist_elem_free(dest);
	}
}

void
epic_filer_go_forward(FilerWindow *filer_window)
{
	struct EpicHistElem *dest = get_elem(filer_window, EPIC_HIST_FORWARD);
	if (dest) {
		filer_change_to(filer_window, dest->fullpath, dest->cursor);
		epic_hist_elem_free(dest);
	}
}

void epic_history_push_elem(GQueue *history, struct EpicHistElem *elem) {
	if (history) {
		if (history->length > EPIC_MAX_HISTORY_LENGTH) {
			g_free(g_queue_pop_head(history));
		}
		g_queue_push_tail(history, elem);
	}
}

/* This function is called every time a filer window changes dir.
 * We add the dir it was before changing to a history list */
void epic_history_add(FilerWindow *fw, gchar *path, gchar *cursor) {
	GQueue *history = NULL;
	struct EpicHistData *data = NULL;

	if (epic_history_datas) {
		data = g_hash_table_lookup(epic_history_datas, fw);
	}
	if (!data) {
		return;
	}

	/* We decide where to add the item depending on the source */
	switch (data->add_source) {
	case EPIC_HIST_BACK:
		history = data->next_history;
		break;
	case EPIC_HIST_OTHER:
		if (data->next_history) {
			epic_history_clear(data->next_history);
		}
		/* EPIC FALLTHROUGH */
	case EPIC_HIST_FORWARD:
		history = data->prev_history;
		break;
	case EPIC_HIST_MENU_BACK:
	case EPIC_HIST_MENU_FORWARD:
		if (data->dummy) {
			epic_hist_elem_set_path(data->dummy, path);
			data->dummy->cursor = cursor ? g_strdup(cursor) : NULL;
		}
		break;
	}

	if (history) {
		epic_history_push_elem(history, epic_hist_elem_new(fw, history, path, cursor));
	}

	data->add_source = EPIC_HIST_OTHER; /* Reset for the next action */
}

/* This one is for when a history menu item is clicked */
void epic_history_activate(GtkMenuShell *item, struct EpicHistElem *elem) {
	int i;
	GQueue *hist_target = NULL;
	struct EpicHistData *data = NULL;

	if (epic_history_datas) {
		data = g_hash_table_lookup(epic_history_datas, elem->fw);
	}
	if (!elem || !data) { /* Is this even possible? */
		return;
	}

	if (elem->history == data->prev_history) {
		data->add_source = EPIC_HIST_MENU_BACK;
		hist_target = data->next_history;
	} else if (elem->history == data->next_history) {
		data->add_source = EPIC_HIST_MENU_FORWARD;
		hist_target = data->prev_history;
	} else {
		hist_target = NULL;
	}

	/* We insert a temp dummy for the current dir, which we don't know yet */
	if (hist_target) {
		data->dummy = epic_hist_elem_new(elem->fw, hist_target, "ERROR???", NULL);
		epic_history_push_elem(hist_target, data->dummy);
	}

	/* Move all elements on top of the clicked one to the other history */
	for (i = 0; i < elem->postmp && elem->history->length; ++i) {
		struct EpicHistElem *e = g_queue_pop_tail(elem->history);
		if (hist_target) {
			e->history = hist_target;
			epic_history_push_elem(hist_target, e);
		} else {
			epic_hist_elem_free(e);
		}
	}
	if (elem->history->length) {
		g_queue_pop_tail(elem->history);
	}
	filer_change_to(elem->fw, elem->fullpath, elem->cursor);
	epic_hist_elem_free(elem);
}

/* Build a menu with the items from a history list */
static GtkWidget *epic_history_build_menu(FilerWindow *filer_window, GQueue *history)
{
	GtkWidget *menu, *item;
	struct EpicHistElem *elem;
	int i, n = history->length;

	menu = gtk_menu_new();

	if (!history || n == 0) {
		item = gtk_menu_item_new_with_label("(empty)");
		gtk_widget_set_sensitive(item, FALSE);
		gtk_widget_show(item);
		gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
		return menu;
	}

	for (i = 0; i < n && i < EPIC_MAX_HISTORY_MENU_LENGTH; ++i) {
		elem = g_queue_peek_nth(history, n - i - 1);
		elem->postmp = i;
		item = gtk_menu_item_new_with_label(elem->display);
		g_signal_connect(item, "activate",
				G_CALLBACK(epic_history_activate),
				elem);
		gtk_widget_show(item);
		gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
	}
	return menu;
}

/* This function positions the menu at the top left corner.
 * I didn't write this. */
static void epic_position_menu(GtkMenu *menu, gint *x, gint *y,
		   	  gboolean *push_in, gpointer data)
{
	FilerWindow *filer_window = (FilerWindow *) data;
	
	gdk_window_get_origin(GTK_WIDGET(filer_window->view)->window, x, y);
}

/* Show a history menu */
void epic_history_show_menu(FilerWindow *filer_window, GQueue *history) {
	GdkEvent *event;
	GtkMenu *menu;
	int	button = 0;

	event = gtk_get_current_event();
	if (event)
	{
		if (event->type == GDK_BUTTON_RELEASE ||
		    event->type == GDK_BUTTON_PRESS)
			button = ((GdkEventButton *) event)->button;
		gdk_event_free(event);
	}

	menu = GTK_MENU(epic_history_build_menu(filer_window, history));
	gtk_menu_popup(menu, NULL, NULL, epic_position_menu, filer_window,
			button, gtk_get_current_event_time());
}

/* Exported functions to show the history menu of a filer window */
void history_show_prev_menu(FilerWindow *fw) {
	struct EpicHistData *data = NULL;

	if (epic_history_datas && (data = g_hash_table_lookup(epic_history_datas, fw))) {
		epic_history_show_menu(fw, data->prev_history);
	}
}

void history_show_next_menu(FilerWindow *fw) {
	struct EpicHistData *data = NULL;

	if (epic_history_datas && (data = g_hash_table_lookup(epic_history_datas, fw))) {
		epic_history_show_menu(fw, data->next_history);
	}
}
