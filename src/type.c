/*
 * ROX-Filer, filer for the ROX desktop project
 * Copyright (C) 2006, Thomas Leonard and others (see changelog for details).
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* type.c - code for dealing with filetypes */

#include "config.h"

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <ctype.h>
#include <time.h>
#include <sys/param.h>
#include <fnmatch.h>
#include <sys/types.h>
#include <fcntl.h>

#include "global.h"

#include "string.h"
#include "fscache.h"
#include "main.h"
#include "pixmaps.h"
#include "run.h"
#include "gui_support.h"
#include "choices.h"
#include "type.h"
#include "support.h"
#include "diritem.h"
#include "dnd.h"
#include "options.h"
#include "filer.h"
#include "action.h"		/* (for action_chmod) */
#include "xml.h"
#include "dropbox.h"
#include "xdgmime.h"
#include "xtypes.h"
#include "run.h"

#define TYPE_NS "http://www.freedesktop.org/standards/shared-mime-info"
enum {SET_MEDIA, SET_TYPE};

/* Colours for file types (same order as base types) */
static gchar *opt_type_colours[][2] = {
	{"display_err_colour",  "#ff0000"},
	{"display_unkn_colour", "#000000"},
	{"display_dir_colour",  "#000080"},
	{"display_pipe_colour", "#444444"},
	{"display_sock_colour", "#ff00ff"},
	{"display_file_colour", "#000000"},
	{"display_cdev_colour", "#000000"},
	{"display_bdev_colour", "#000000"},
	{"display_door_colour", "#ff00ff"},
	{"display_exec_colour", "#006000"},
	{"display_adir_colour", "#006000"}
};
#define NUM_TYPE_COLOURS\
		(sizeof(opt_type_colours) / sizeof(opt_type_colours[0]))

/* Parsed colours for file types */
static Option o_type_colours[NUM_TYPE_COLOURS];
static GdkColor	type_colours[NUM_TYPE_COLOURS];

/* Static prototypes */
static void alloc_type_colours(void);
static void options_changed(void);
static MIME_type *get_mime_type(const gchar *type_name, gboolean can_create);
static void set_icon_theme(void);
static GList *build_icon_theme(Option *option, xmlNode *node, guchar *label);

/* Hash of all allocated MIME types, indexed by "media/subtype".
 * MIME_type structs are never freed; this table prevents memory leaks
 * when rereading the config files.
 */
static GHashTable *type_hash = NULL;

/* Most things on Unix are text files, so this is the default type */
MIME_type *text_plain;
MIME_type *inode_directory;
MIME_type *inode_mountpoint;
MIME_type *inode_pipe;
MIME_type *inode_socket;
MIME_type *inode_block_dev;
MIME_type *inode_char_dev;
MIME_type *application_executable;
MIME_type *application_octet_stream;
MIME_type *application_x_shellscript;
MIME_type *application_x_desktop;
MIME_type *inode_unknown;
MIME_type *inode_door;

static Option o_display_colour_types;
static Option o_icon_theme;

static GtkIconTheme *icon_theme = NULL;
static GtkIconTheme *rox_theme = NULL;
static GtkIconTheme *gnome_theme = NULL;

void type_init(void)
{
	int	    i;

	icon_theme = gtk_icon_theme_new();
	
	type_hash = g_hash_table_new(g_str_hash, g_str_equal);

	text_plain = get_mime_type("text/plain", TRUE);
	inode_directory = get_mime_type("inode/directory", TRUE);
	inode_mountpoint = get_mime_type("inode/mount-point", TRUE);
	inode_pipe = get_mime_type("inode/fifo", TRUE);
	inode_socket = get_mime_type("inode/socket", TRUE);
	inode_block_dev = get_mime_type("inode/blockdevice", TRUE);
	inode_char_dev = get_mime_type("inode/chardevice", TRUE);
	application_executable = get_mime_type("application/x-executable", TRUE);
	application_octet_stream = get_mime_type("application/octet-stream", TRUE);
	application_x_shellscript = get_mime_type("application/x-shellscript", TRUE);
	application_x_desktop = get_mime_type("application/x-desktop", TRUE);
	application_x_desktop->executable = TRUE;
	inode_unknown = get_mime_type("inode/unknown", TRUE);
	inode_door = get_mime_type("inode/door", TRUE);

	option_add_string(&o_icon_theme, "icon_theme", "ROX");
	option_add_int(&o_display_colour_types, "display_colour_types", TRUE);
	option_register_widget("icon-theme-chooser", build_icon_theme);
	
	for (i = 0; i < NUM_TYPE_COLOURS; i++)
		option_add_string(&o_type_colours[i],
				  opt_type_colours[i][0],
				  opt_type_colours[i][1]);
	alloc_type_colours();

	set_icon_theme();

	option_add_notify(options_changed);
}

/* Read-load all the glob patterns.
 * Note: calls filer_update_all.
 */
void reread_mime_files(void)
{
	gtk_icon_theme_rescan_if_needed(icon_theme);

	xdg_mime_shutdown();

	filer_update_all();
}

/* Returns the MIME_type structure for the given type name. It is looked
 * up in type_hash and returned if found. If not found (and can_create is
 * TRUE) then a new MIME_type is made, added to type_hash and returned.
 * NULL is returned if type_name is not in type_hash and can_create is
 * FALSE, or if type_name does not contain a '/' character.
 */
static MIME_type *get_mime_type(const gchar *type_name, gboolean can_create)
{
        MIME_type *mtype;
	gchar *slash;

	mtype = g_hash_table_lookup(type_hash, type_name);
	if (mtype || !can_create)
		return mtype;

	slash = strchr(type_name, '/');
	if (slash == NULL)
	{
		g_warning("MIME type '%s' does not contain a '/' character!",
			  type_name);
		return NULL;
	}

	mtype = g_new(MIME_type, 1);
	mtype->media_type = g_strndup(type_name, slash - type_name);
	mtype->subtype = g_strdup(slash + 1);
	mtype->image = NULL;
	mtype->comment = NULL;

	mtype->executable = xdg_mime_mime_type_subclass(type_name,
						"application/x-executable");

	g_hash_table_insert(type_hash, g_strdup(type_name), mtype);

	return mtype;
}

const char *basetype_name(DirItem *item)
{
	if (item->flags & ITEM_FLAG_SYMLINK)
		return _("Sym link");
	else if (item->flags & ITEM_FLAG_MOUNT_POINT)
		return _("Mount point");
	else if (item->flags & ITEM_FLAG_APPDIR)
		return _("App dir");

	switch (item->base_type)
	{
		case TYPE_FILE:
			return _("File");
		case TYPE_DIRECTORY:
			return _("Dir");
		case TYPE_CHAR_DEVICE:
			return _("Char dev");
		case TYPE_BLOCK_DEVICE:
			return _("Block dev");
		case TYPE_PIPE:
			return _("Pipe");
		case TYPE_SOCKET:
			return _("Socket");
		case TYPE_DOOR:
			return _("Door");
	}
	
	return _("Unknown");
}

struct mime_list {
	GList *list;
	gboolean only_regular;
};

static void append_names(gpointer key, gpointer value, gpointer udata)
{
	struct mime_list *mlist = (struct mime_list*) udata;

	if(!mlist->only_regular || strncmp((char *)key, "inode/", 6)!=0)
		mlist->list = g_list_prepend(mlist->list, key);
}

/* Return list of all mime type names. Caller must free the list
 * but NOT the strings it contains (which are never freed).
 If only_regular is true then inode types are excluded.
 */
GList *mime_type_name_list(gboolean only_regular)
{
	struct mime_list list;

	list.list=NULL;
	list.only_regular=only_regular;
		
	g_hash_table_foreach(type_hash, append_names, &list);
	list.list = g_list_sort(list.list, (GCompareFunc) strcmp);

	return list.list;
}

/*			MIME-type guessing 			*/

/* Get the type of this file - stats the file and uses that if
 * possible. For regular or missing files, uses the pathname.
 */
MIME_type *type_get_type(const guchar *path)
{
	DirItem		*item;
	MIME_type	*type = NULL;

	item = diritem_new("");
	diritem_restat(path, item, NULL);
	if (item->base_type != TYPE_ERROR)
		type = item->mime_type;
	diritem_free(item);

	if (type)
		return type;

	type = type_from_path(path);

	if (!type)
		return text_plain;

	return type;
}

/* Returns a pointer to the MIME-type.
 *
 * Tries all enabled methods:
 * - Look for extended attribute
 * - If no attribute, check file name
 * - If no name rule, check contents
 *
 * NULL if we can't think of anything.
 */
MIME_type *type_from_path(const char *path)
{
	MIME_type *mime_type = NULL;
	const char *type_name;

	/* Check for extended attribute first */
	mime_type = xtype_get(path);
	if (mime_type)
		return mime_type;

	/* Try name and contents next */
	type_name = xdg_mime_get_mime_type_for_file(path, NULL);
	if (type_name)
		return get_mime_type(type_name, TRUE);

	return NULL;
}

/* Returns the file/dir in Choices for handling this type.
 * NULL if there isn't one. g_free() the result.
 */
GAppInfo *handler_for(MIME_type *type)
{
	char	*type_name;
	GAppInfo *appinfo;

	type_name = g_strconcat(type->media_type, "/", type->subtype, NULL);
	appinfo = g_app_info_get_default_for_type(type_name, FALSE);
	g_free(type_name);

	return appinfo;
}

MIME_type *mime_type_lookup(const char *type)
{
	return get_mime_type(type, TRUE);
}

static void init_aux_theme(GtkIconTheme **ptheme, const char *name)
{
	if (*ptheme)
		return;
	*ptheme = gtk_icon_theme_new();
	gtk_icon_theme_set_custom_theme(*ptheme, name);
}

inline static void init_rox_theme(void)
{
	init_aux_theme(&rox_theme, "ROX");
}

inline static void init_gnome_theme(void)
{
	gchar *name;
	g_object_get(gtk_settings_get_default(), "gtk-icon-theme-name", &name, NULL);
	init_aux_theme(&gnome_theme, name ? name : "gnome");
}

/* We don't want ROX to override configured theme so try all possibilities
 * in icon_theme first */
static GtkIconInfo *mime_type_lookup_icon_info(GtkIconTheme *theme,
		MIME_type *type, gint epic_size, char **final_name)
{
	char *type_name = g_strconcat(type->media_type, "-", type->subtype, NULL);
	GtkIconInfo *full = gtk_icon_theme_lookup_icon(theme, type_name, epic_size,
						GTK_ICON_LOOKUP_GENERIC_FALLBACK);

	if (!full)
	{
		/* Ugly hack... try for a GNOME icon */
		if (type == inode_directory)
			type_name = g_strdup("gnome-fs-directory");
		else
			type_name = g_strconcat("gnome-mime-", type->media_type,
					"-", type->subtype, NULL);
		full = gtk_icon_theme_lookup_icon(theme, type_name, epic_size, 0);
	}
	if (!full)
	{
		/* Try for a media type */
		type_name = g_strconcat(type->media_type, "-x-generic", NULL);
		full = gtk_icon_theme_lookup_icon(theme, type_name, epic_size, 0);
	}
	if (!full)
	{
		/* Ugly hack... try for a GNOME default media icon */
		type_name = g_strconcat("gnome-mime-", type->media_type, NULL);

		full = gtk_icon_theme_lookup_icon(theme, type_name, epic_size, 0);
	}
	if (!full && strcmp(type->media_type, "inode") != 0)
	{
		type_name = g_strdup("gtk-file");
		full = gtk_icon_theme_lookup_icon(theme, type_name, epic_size,0);
	}
	*final_name = type_name;
	return full;
}

/*			Actions for types 			*/

/* Return the image for this type, loading it if needed.
 * Places to check are: (eg type="text_plain", base="text")
 * 1. <Choices>/MIME-icons/base_subtype
 * 2. Icon theme 'mime-base:subtype'
 * 3. Icon theme 'mime-base'
 * 4. Unknown type icon.
 *
 * Special case: If an icon cannot be found for inode/mount-point, the icon for
 * inode/directory will be returned (if possible).
 *
 * Note: You must g_object_unref() the image afterwards.
 */
MaskedPixmap *type_to_icon(MIME_type *type)
{
	GtkIconInfo *full;
	char	*type_name, *path, *in;
	time_t	now;
	GtkIconTheme *epic_theme = NULL;

	if (type == NULL)
	{
		g_object_ref(im_unknown);
		return im_unknown;
	}

	now = time(NULL);
	/* Already got an image? */
	if (type->image)
	{
		/* Yes - don't recheck too often */
		if (abs(now - type->image_time) < 2)
		{
			g_object_ref(type->image);
			return type->image;
		}
		g_object_unref(type->image);
		type->image = NULL;
	}

again:
	type_name = g_strconcat(type->media_type, "_", type->subtype,
				".png", NULL);
	path = choices_find_xdg_path_load(type_name, "MIME-icons", SITE);
	g_free(type_name);
	if (path)
	{
		type->image = g_fscache_lookup(pixmap_cache, path);
		g_free(path);
	}

	if (type->image)
		goto out;

	full = mime_type_lookup_icon_info(epic_theme = icon_theme, type, ICON_HEIGHT, &in);
	if (!full && type == inode_mountpoint)
	{
		/* Try to use the inode/directory icon for inode/mount-point */
		type = inode_directory;
		goto again;
	}
	if (!full && icon_theme != gnome_theme)
	{
		init_gnome_theme();
		full = mime_type_lookup_icon_info(epic_theme = gnome_theme, type, ICON_HEIGHT, &in);
	}
	if (!full && icon_theme != rox_theme)
	{
		init_rox_theme();
		full = mime_type_lookup_icon_info(epic_theme = rox_theme, type, ICON_HEIGHT, &in);
	}
	if (full)
	{
		const char *icon_path;
		/* Get the actual icon through our cache, not through GTK, because
		 * GTK doesn't cache icons.
		 */
		icon_path = gtk_icon_info_get_filename(full);
		if (icon_path != NULL)
			type->image = g_fscache_lookup(pixmap_cache, icon_path);
		/* else shouldn't happen, because we didn't use
		 * GTK_ICON_LOOKUP_USE_BUILTIN.
		 */
		gtk_icon_info_free(full);

		if (type->image) {
			type->image->epic_theme = epic_theme;
			type->image->icon_name = in;
		}

	}

out:
	if (!type->image)
	{
		/* One ref from the type structure, one returned */
		type->image = im_unknown;
		g_object_ref(im_unknown);
	}

	type->image_time = now;
	
	g_object_ref(type->image);
	return type->image;
}

MaskedPixmap *epic_icon_from_string(char *str)
{
	GtkIconInfo *full;
	GtkIconTheme *epic_theme = NULL;
	MaskedPixmap *image = NULL;

	full = gtk_icon_theme_lookup_icon(epic_theme = icon_theme, str, ICON_HEIGHT,
	                                  GTK_ICON_LOOKUP_GENERIC_FALLBACK);
	if (!full && icon_theme != gnome_theme)
	{
		init_gnome_theme();
		full = gtk_icon_theme_lookup_icon(epic_theme = gnome_theme, str, ICON_HEIGHT,
		                                  GTK_ICON_LOOKUP_GENERIC_FALLBACK);
	}
	if (!full && icon_theme != rox_theme)
	{
		init_rox_theme();
		full = gtk_icon_theme_lookup_icon(epic_theme = rox_theme, str, ICON_HEIGHT,
		                                  GTK_ICON_LOOKUP_GENERIC_FALLBACK);
	}
	if (full)
	{
		const char *icon_path;
		icon_path = gtk_icon_info_get_filename(full);
		if (icon_path != NULL)
			image = g_fscache_lookup(pixmap_cache, icon_path);
		gtk_icon_info_free(full);

		if (image) {
			image->epic_theme = epic_theme;
			image->icon_name = g_strdup(str);
		}

	}
	return image;
}

GdkAtom type_to_atom(MIME_type *type)
{
	char	*str;
	GdkAtom	retval;
	
	g_return_val_if_fail(type != NULL, GDK_NONE);

	str = g_strconcat(type->media_type, "/", type->subtype, NULL);
	retval = gdk_atom_intern(str, FALSE);
	g_free(str);
	
	return retval;
}

/* Used to set the default application to "app" for "type_name" mime type
 */
static void set_default_app(const guchar *app, const guchar *type_name)
{
	GAppInfo *appinfo = NULL;
	GList *list, *elem;
	list = g_app_info_get_all();
	if (list) {
		for (elem = list; elem; elem = elem->next) {
			if (strcmp(app, g_app_info_get_id(elem->data)) == 0) {
				appinfo = g_app_info_dup(elem->data);
			}
			g_object_unref(elem->data);
		}
		g_list_free(list);
	}
	if (appinfo) {
		g_app_info_set_as_default_for_type(appinfo, type_name, NULL);
		g_free(appinfo);
	} else {
		delayed_error(_("This is not a program! Give me an application "
		                "instead!"));
	}
}

static void set_action_response(GtkWidget *dialog, gint response, gpointer data)
{
	if (response == GTK_RESPONSE_OK) {
		const guchar *default_app;
		default_app = gtk_button_get_label(g_object_get_data(G_OBJECT(dialog), "current-epic-selection"));
		if (default_app) {
			MIME_type *type;
			guchar *type_name; 
			type = g_object_get_data(G_OBJECT(dialog), "mime_type");
			type_name = g_strconcat(type->media_type, "/", type->subtype, NULL);
			set_default_app(default_app, type_name);
			g_free(type_name);
		}
	}
	gtk_widget_destroy(dialog);
}

static void button_pressed_epically(GtkWidget *w, gpointer data)
{
	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(w))) {
		GtkDialog *dialog;
		GtkWidget *oldw;
		dialog = GTK_DIALOG(data);
		oldw = g_object_get_data(G_OBJECT(dialog), "current-epic-selection");
		if (oldw) {
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(oldw), FALSE);
		}
		g_object_set_data(G_OBJECT(dialog), "current-epic-selection", GTK_BUTTON(w));
	}
}

/* Find the current command which is used to run files of this type,
 * and return a textual description of it.
 * Only call for non-executable files.
 * g_free() the result.
 */
gchar *describe_current_command(MIME_type *type)
{
	GAppInfo *handler;
	char *desc;

	g_return_val_if_fail(type != NULL, NULL);

	handler = handler_for(type);

	if (!handler)
		return g_strdup(_("No run action defined"));

	desc = g_strdup(g_app_info_get_name(handler));
	g_free(handler);
	return desc;
}

/* Display a dialog box allowing the user to set the default run action
 * for this type.
 */
void type_set_handler_dialog(MIME_type *type)
{
	guchar		*tmp;
	GtkDialog	*dialog;
	GtkWidget	*label;
	GtkWidget	*hbox;
	GList		*list, *listprio, *elem;

	g_return_if_fail(type != NULL);

	dialog = GTK_DIALOG(gtk_dialog_new());
	gtk_dialog_set_has_separator(dialog, FALSE);
	gtk_window_set_position(GTK_WINDOW(dialog), GTK_WIN_POS_MOUSE);

	g_object_set_data(G_OBJECT(dialog), "mime_type", type);

	gtk_window_set_title(GTK_WINDOW(dialog), _("Set run action"));

	tmp = g_strconcat("Set run action for ", type->media_type, "/", type->subtype, NULL);
	label = gtk_label_new(tmp);
	g_free(tmp);
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0);
	gtk_box_pack_start(GTK_BOX(dialog->vbox), label, FALSE, FALSE, 4);

#define NEW_BUTTON do { \
	GtkWidget *button; \
	GIcon *icon; \
	button = gtk_toggle_button_new_with_label(g_app_info_get_id(elem->data)); \
	icon = g_app_info_get_icon(elem->data); \
	if (icon) { \
		GtkIconInfo *iconinfo; \
		GdkPixbuf *pb; \
		GtkWidget *image; \
		iconinfo = gtk_icon_theme_lookup_by_gicon(icon_theme, icon, 32 * epic_screen_scale, \
		                                          GTK_ICON_LOOKUP_FORCE_SIZE); \
		if (!iconinfo) { \
			iconinfo = gtk_icon_theme_lookup_icon(icon_theme, "application-x-executable", \
			                                      32 * epic_screen_scale, \
			                                      GTK_ICON_LOOKUP_FORCE_SIZE); \
		} \
		pb = gtk_icon_info_load_icon(iconinfo, NULL); \
		gtk_icon_info_free(iconinfo); \
		image = gtk_image_new_from_pixbuf(pb); \
		g_object_unref(pb); \
		gtk_button_set_image(GTK_BUTTON(button), image); \
	} \
	gtk_button_set_alignment(GTK_BUTTON(button), 0, 0); \
	if (default_app && strcmp(gtk_button_get_label(GTK_BUTTON(button)), default_app_name) == 0) { \
		g_object_set_data(G_OBJECT(dialog), "current-epic-selection", button); \
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE); \
	} \
	gtk_table_attach(GTK_TABLE(frame), button, 0, 1, i, i + 1, GTK_FILL, GTK_FILL, 0, 0); \
	g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK(button_pressed_epically), dialog); \
	i++; \
} while (0)

	list = g_app_info_get_all();
	if (list) {
		int i = 0;
		GtkWidget *frame, *scrollwin;
		GAppInfo *default_app;
		const char *default_app_name;
		scrollwin = gtk_scrolled_window_new(NULL, NULL);
		frame = gtk_table_new(g_list_length(list), 1, FALSE);
		tmp = g_strconcat(type->media_type, "/", type->subtype, NULL);
		listprio = g_app_info_get_all_for_type(tmp);
		default_app = g_app_info_get_default_for_type(tmp, FALSE);
		if (default_app) {
			default_app_name = g_app_info_get_id(default_app);
		}
		g_free(tmp);
		if (listprio) {
			for (elem = listprio; elem; elem = elem->next) {
				NEW_BUTTON;
			}
		}
		for (elem = list; elem; elem = elem->next) {
			int add = 1;
			if (listprio) {
				GList *elem2;
				for (elem2 = listprio; elem2; elem2 = elem2->next) {
					if (strcmp(g_app_info_get_id(elem2->data),
					           g_app_info_get_id(elem->data)) == 0) {
						add = 0;
						break;
					}
				}
			}
			if (add) {
				NEW_BUTTON;
			}
			g_object_unref(elem->data);
		}
		if (listprio) {
			for (elem = listprio; elem; elem = elem->next) {
				g_object_unref(elem->data);
			}
			g_list_free(listprio);
		}
		if (default_app) {
			g_free(default_app);
		}
		g_list_free(list);
		gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollwin), frame);
		gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollwin), GTK_POLICY_NEVER, GTK_POLICY_ALWAYS);
		gtk_widget_set_size_request(scrollwin, -1, 200 * epic_screen_scale);
		gtk_box_pack_start(GTK_BOX(dialog->vbox), scrollwin, TRUE, TRUE, 4);
	}

#undef NEW_BUTTON

	gtk_dialog_add_button(dialog, GTK_STOCK_CLOSE, GTK_RESPONSE_CANCEL);
	gtk_dialog_add_button(dialog, GTK_STOCK_OK, GTK_RESPONSE_OK);

	hbox = gtk_hbox_new(TRUE, 4);
	gtk_box_pack_start(GTK_BOX(dialog->vbox), hbox, FALSE, TRUE, 0);

	gtk_dialog_set_default_response(dialog, GTK_RESPONSE_OK);
	
	g_signal_connect(dialog, "response",
			G_CALLBACK(set_action_response), NULL);

	gtk_widget_show_all(GTK_WIDGET(dialog));
}

MIME_type *mime_type_from_base_type(int base_type)
{
	switch (base_type)
	{
		case TYPE_FILE:
			return text_plain;
		case TYPE_DIRECTORY:
			return inode_directory;
		case TYPE_PIPE:
			return inode_pipe;
		case TYPE_SOCKET:
			return inode_socket;
		case TYPE_BLOCK_DEVICE:
			return inode_block_dev;
		case TYPE_CHAR_DEVICE:
			return inode_char_dev;
	        case TYPE_DOOR:
	                return inode_door;
	}
	return inode_unknown;
}

/* Takes the st_mode field from stat() and returns the base type.
 * Should not be a symlink.
 */
int mode_to_base_type(int st_mode)
{
	if (S_ISREG(st_mode))
		return TYPE_FILE;
	else if (S_ISDIR(st_mode))
		return TYPE_DIRECTORY;
	else if (S_ISBLK(st_mode))
		return TYPE_BLOCK_DEVICE;
	else if (S_ISCHR(st_mode))
		return TYPE_CHAR_DEVICE;
	else if (S_ISFIFO(st_mode))
		return TYPE_PIPE;
	else if (S_ISSOCK(st_mode))
		return TYPE_SOCKET;
	else if (S_ISDOOR(st_mode))
		return TYPE_DOOR;

	return TYPE_ERROR;
}

/* Returns TRUE is this is something that is run by looking up its type
 * in MIME-types and, hence, can have its run action set.
 */
gboolean can_set_run_action(DirItem *item)
{
	g_return_val_if_fail(item != NULL, FALSE);

	return item->base_type == TYPE_FILE && !EXECUTABLE_FILE(item);
}

/* Parse file type colours and allocate/free them as necessary */
static void alloc_type_colours(void)
{
	gboolean	success[NUM_TYPE_COLOURS];
	int		change_count = 0;	/* No. needing realloc */
	int		i;
	static gboolean	allocated = FALSE;

	/* Parse colours */
	for (i = 0; i < NUM_TYPE_COLOURS; i++)
	{
		GdkColor *c = &type_colours[i];
		gushort r = c->red;
		gushort g = c->green;
		gushort b = c->blue;

		gdk_color_parse(o_type_colours[i].value, &type_colours[i]);

		if (allocated && (c->red != r || c->green != g || c->blue != b))
			change_count++;
	}
	
	/* Free colours if they were previously allocated and
	 * have changed or become unneeded.
	 */
	if (allocated && (change_count || !o_display_colour_types.int_value))
	{
		gdk_colormap_free_colors(gdk_rgb_get_colormap(),
					 type_colours, NUM_TYPE_COLOURS);
		allocated = FALSE;
	}

	/* Allocate colours, unless they are still allocated (=> they didn't
	 * change) or we don't want them anymore.
	 * XXX: what should be done if allocation fails?
	 */
	if (!allocated && o_display_colour_types.int_value)
	{
		gdk_colormap_alloc_colors(gdk_rgb_get_colormap(),
				type_colours, NUM_TYPE_COLOURS,
				FALSE, TRUE, success);
		allocated = TRUE;
	}
}

static void expire_timer(gpointer key, gpointer value, gpointer data)
{
	MIME_type *type = value;

	type->image_time = 0;
}

static void options_changed(void)
{
	alloc_type_colours();
	if (o_icon_theme.has_changed)
	{
		set_icon_theme();
		g_hash_table_foreach(type_hash, expire_timer, NULL);
		full_refresh();
	}
}

/* Return a pointer to a (static) colour for this item. If colouring is
 * off, returns normal.
 */
GdkColor *type_get_colour(DirItem *item, GdkColor *normal)
{
	int type = item->base_type;

	if (!o_display_colour_types.int_value)
		return normal;

	if (EXECUTABLE_FILE(item))
		type = TYPE_EXEC;
	else if (item->flags & ITEM_FLAG_APPDIR)
		type = TYPE_APPDIR;

	g_return_val_if_fail(type >= 0 && type < NUM_TYPE_COLOURS, normal);

	return &type_colours[type];
}

static char **get_xdg_data_dirs(int *n_dirs)
{
	const char *env;
	char **dirs;
	int i, n;

	env = getenv("XDG_DATA_DIRS");
	if (!env)
		env = "/usr/local/share/:/usr/share/";
	dirs = g_strsplit(env, ":", 0);
	g_return_val_if_fail(dirs != NULL, NULL);
	for (n = 0; dirs[n]; n++)
		;
	for (i = n; i > 0; i--)
		dirs[i] = dirs[i - 1];
	env = getenv("XDG_DATA_HOME");
	if (env)
		dirs[0] = g_strdup(env);
	else
		dirs[0] = g_build_filename(g_get_home_dir(), ".local",
					   "share", NULL);
	*n_dirs = n + 1;
	return dirs;
}

/* Try to fill in 'type->comment' from this document */
static void get_comment(MIME_type *type, const guchar *path)
{
	xmlNode *node;
	XMLwrapper *doc;
	
	doc = xml_cache_load(path);
	if (!doc)
		return;

	node = xml_get_section(doc, TYPE_NS, "comment");

	if (node)
	{
		char *val;
		g_return_if_fail(type->comment == NULL);
		val= xmlNodeListGetString(node->doc, node->xmlChildrenNode, 1);
		type->comment = g_strdup(val);
		xmlFree(val);
	}

	g_object_unref(doc);
}

/* Fill in the comment field for this MIME type */
static void find_comment(MIME_type *type)
{
	char **dirs;
	int i, n_dirs = 0;

	if (type->comment)
	{
		g_free(type->comment);
		type->comment = NULL;
	}

	dirs = get_xdg_data_dirs(&n_dirs);
	g_return_if_fail(dirs != NULL);

	for (i = 0; i < n_dirs; i++)
	{
		guchar *path;
		
		path = g_strdup_printf("%s/mime/%s/%s.xml", dirs[i],
				type->media_type, type->subtype);
		get_comment(type, path);
		g_free(path);
		if (type->comment)
			break;
	}

	if (!type->comment)
		type->comment = g_strdup_printf("%s/%s", type->media_type,
						type->subtype);

	for (i = 0; i < n_dirs; i++)
		g_free(dirs[i]);
	g_free(dirs);
}

const char *mime_type_comment(MIME_type *type)
{
	if (!type->comment)
		find_comment(type);

	return type->comment;
}

static void unref_icon_theme(void)
{
	if (icon_theme && icon_theme != rox_theme && icon_theme != gnome_theme)
		g_object_unref(icon_theme);
}

static void set_icon_theme(void)
{
	struct stat info;
	char *icon_home;
	const char *theme_dir;
	const char *theme_name = o_icon_theme.value;

	if (!theme_name || !*theme_name)
		theme_name = "ROX";

	if (!strcmp(theme_name, "ROX"))
	{
		unref_icon_theme();
		init_rox_theme();
		icon_theme = rox_theme;
	}
	else if (!strcmp(theme_name, "gnome"))
	{
		unref_icon_theme();
		init_gnome_theme();
		icon_theme = gnome_theme;
	}
	else
	{
		if (icon_theme == rox_theme || icon_theme == gnome_theme)
			icon_theme = gtk_icon_theme_new();
		gtk_icon_theme_set_custom_theme(icon_theme, theme_name);
	}

	/* Ensure the ROX theme exists. */

	if (stat(DATAROOTDIR "/icons/ROX", &info) == 0)
		return;	/* Already exists globally, don't create anything please*/

	icon_home = g_build_filename(home_dir, ".icons", "ROX", NULL);
	if (stat(icon_home, &info) == 0)
		return;	/* Already exists */

	/* First, create the .icons directory */
	theme_dir = make_path(home_dir, ".icons");
	if (!file_exists(theme_dir))
		mkdir(theme_dir, 0755);

	if (lstat(icon_home, &info) == 0)
	{
		/* Probably a broken symlink, then. Remove it. */
		if (unlink(icon_home))
			g_warning("Error removing broken symlink %s: %s", icon_home, g_strerror(errno));
		else
			g_warning("Removed broken symlink %s", icon_home);
	}

	if (symlink(make_path(app_dir, "ROX"), icon_home))
	{
		delayed_error(_("Failed to create symlink '%s':\n%s"), icon_home, g_strerror(errno));
		open_to_show(icon_home);
	}
	g_free(icon_home);

	gtk_icon_theme_rescan_if_needed(icon_theme);
}

static guchar *read_theme(Option *option)
{
	GtkOptionMenu *om = GTK_OPTION_MENU(option->widget);
	GtkLabel *item;

	item = GTK_LABEL(GTK_BIN(om)->child);

	g_return_val_if_fail(item != NULL, g_strdup("ROX"));

	return g_strdup(gtk_label_get_text(item));
}

static void update_theme(Option *option)
{
	GtkOptionMenu *om = GTK_OPTION_MENU(option->widget);
	GtkWidget *menu;
	GList *kids, *next;
	int i = 0;

	menu = gtk_option_menu_get_menu(om);

	kids = gtk_container_get_children(GTK_CONTAINER(menu));
	for (next = kids; next; next = next->next, i++)
	{
		GtkLabel *item = GTK_LABEL(GTK_BIN(next->data)->child);
		const gchar *label;

		/* The label actually moves from the menu!! */
		if (!item)
			item = GTK_LABEL(GTK_BIN(om)->child);

		label = gtk_label_get_text(item);

		g_return_if_fail(label != NULL);

		if (strcmp(label, option->value) == 0)
			break;
	}
	g_list_free(kids);
	
	if (next)
		gtk_option_menu_set_history(om, i);
	else
		g_warning("Theme '%s' not found", option->value);
}

static void add_themes_from_dir(GPtrArray *names, const char *dir)
{
	GPtrArray *list;
	int i;

	if (access(dir, F_OK) != 0)
		return;

	list = list_dir(dir);
	g_return_if_fail(list != NULL);

	for (i = 0; i < list->len; i++)
	{
		char *index_path;

		index_path = g_build_filename(dir, list->pdata[i],
						"index.theme", NULL);
		
		if (access(index_path, F_OK) == 0)
			g_ptr_array_add(names, list->pdata[i]);
		else
			g_free(list->pdata[i]);

		g_free(index_path);
	}

	g_ptr_array_free(list, TRUE);
}

static GList *build_icon_theme(Option *option, xmlNode *node, guchar *label)
{
	GtkWidget *button, *menu, *hbox;
	GPtrArray *names;
	gchar **theme_dirs = NULL;
	gint n_dirs = 0;
	int i;

	g_return_val_if_fail(option != NULL, NULL);
	g_return_val_if_fail(label != NULL, NULL);

	hbox = gtk_hbox_new(FALSE, 4);

	gtk_box_pack_start(GTK_BOX(hbox), gtk_label_new(_(label)),
				FALSE, TRUE, 0);

	button = gtk_option_menu_new();
	gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);

	menu = gtk_menu_new();
	gtk_option_menu_set_menu(GTK_OPTION_MENU(button), menu);

	gtk_icon_theme_get_search_path(icon_theme, &theme_dirs, &n_dirs);
	names = g_ptr_array_new();
	for (i = 0; i < n_dirs; i++)
		add_themes_from_dir(names, theme_dirs[i]);
	g_strfreev(theme_dirs);

	g_ptr_array_sort(names, strcmp2);

	for (i = 0; i < names->len; i++)
	{
		GtkWidget *item;
		char *name = names->pdata[i];

		item = gtk_menu_item_new_with_label(name);
		gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
		gtk_widget_show_all(item);

		g_free(name);
	}

	g_ptr_array_free(names, TRUE);

	option->update_widget = update_theme;
	option->read_widget = read_theme;
	option->widget = button;

	gtk_signal_connect_object(GTK_OBJECT(button), "changed",
			GTK_SIGNAL_FUNC(option_check_widget),
			(GtkObject *) option);

	return g_list_append(NULL, hbox);
}

GtkIconInfo *theme_lookup_icon(const gchar *icon_name, gint size,
		GtkIconLookupFlags flags)
{
	GtkIconInfo *result = gtk_icon_theme_lookup_icon(icon_theme,
			icon_name, size, flags);

	if (!result && icon_theme != rox_theme)
	{
		init_rox_theme();
		result = gtk_icon_theme_lookup_icon(rox_theme,
			icon_name, size, flags);
	}
	if (!result && icon_theme != gnome_theme)
	{
		init_gnome_theme();
		result = gtk_icon_theme_lookup_icon(gnome_theme,
			icon_name, size, flags);
	}
	return result;
}

GdkPixbuf *theme_load_icon(const gchar *icon_name, gint size,
		GtkIconLookupFlags flags, GtkIconTheme **epic_theme)
{
	GtkIconTheme *theme = NULL;
	GdkPixbuf *result = gtk_icon_theme_load_icon(theme = icon_theme,
			icon_name, size, flags, NULL);

	if (!result && icon_theme != gnome_theme)
	{
		init_gnome_theme();
		result = gtk_icon_theme_load_icon(theme = gnome_theme,
			icon_name, size, flags, NULL);
	}
	if (!result && icon_theme != rox_theme)
	{
		init_rox_theme();
		result = gtk_icon_theme_load_icon(theme = rox_theme,
			icon_name, size, flags, NULL);
	}
	if (epic_theme) {
		*epic_theme = theme;
	}
	return result;
}
